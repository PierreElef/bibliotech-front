# BibliotechFront

## Couches Techniques
Pour chaque entité, il y a :
- un module général qui appele le router, les components et les autres modules utilisés par le component (module @angular, material) 
- un router qui donne les différentes routes du module, protège les routes s'il y a un middleware et appelle le component
- un component général qui est lié à la route parente
- des components qui remplissent une fonction particulière (formulaire, listing, ...)
- un service qui fait le lien entre le model de l'entité et l'API
- un model qui définit le format des données de l'entité

## Organisation du module
Chaque entité possède : 
- un module, qui charge tous les components et modules dont il a besoin
- un component entité-list qui liste les items du même entité 
- un component entité-item qui affiche les données d'un item de l'entité
- un component entité-create qui affiche la page de création d'un item de l'entité
- un component entité-edit qui affiche la page d'édition d'un item existant de l'entité
- un component entité-form qui permet de remplir un formulaire pour la création et l'édition d'un item

Chaque entité a son module enregistré dans app-routing.module.ts pour qu'il soit tous appelés lors du build.

## Intercepteurs
Lors de l'authentification, le token est stocké dans le localstorage du navigateur. Un intercepteur qui ajoute ce token dans le header des requêtes afin de donner l'autorisation dans l'API.
Un autre intercepteur a été créé pour afficher les erreurs comme le refus d'accéder à certaines routes, ou de faire des requêtes sans authentifications.

## Angular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
