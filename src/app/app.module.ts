import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AuthModule } from './auth/auth.module';
import { HeaderComponent } from './commun/header/header.component';
import { FooterComponent } from './commun/footer/footer.component';
import { DashboardComponent } from './commun/dashboard/dashboard.component';
import { ResourceModule } from './commun/resource/resource.module';
import { MeModule } from './commun/me/me.module';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { InterceptorModule } from './commun/ínterceptor/interceptor.module';
import { HomepageComponent } from './commun/homepage/homepage.component';
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    DashboardComponent,
    HomepageComponent
  ],
  imports: [
    FormsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    ResourceModule,
    AppRoutingModule,
    AuthModule,
    MeModule,
    InterceptorModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
