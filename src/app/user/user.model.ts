export interface User {
  id: number,
  firstName: string;
  lastName: string;
  email: string;
  role: string;
}
export interface DtoUser {
  firstName: string;
  lastName: string;
  email: string;
  role: string;
}
