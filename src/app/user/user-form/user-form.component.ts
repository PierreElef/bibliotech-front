import { Component, Input, OnInit } from '@angular/core';
import { DtoUser, User } from '../user.model';
import { Router } from '@angular/router';
import { UserService } from '../user-service/user.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  @Input() user: User;
  newUser: DtoUser = {
    firstName : '',
    lastName : '',
    email: '',
    role: '',
  };
  firstName:string;
  lastName:string;
  email: string;
  role: string;
  create = false;

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit(): void {
    if (this.user) {
      this.firstName = this.user.firstName;
      this.lastName = this.user.lastName;
      this.email = this.user.email;
      this.role = this.user.role;
    } else {
      this.create = true;
    }
  }

  submit() {
    if (this.user) {
      this.user.firstName = this.firstName;
      this.user.lastName = this.lastName;
      this.user.email = this.email;
      this.user.role = this.role;
      this.userService.setUserById(this.user.id, this.user).subscribe();
      this.router.navigate(['/users']);
    } else {
      this.newUser.firstName = this.firstName;
      this.newUser.lastName = this.lastName;
      this.newUser.email = this.email;
      this.newUser.role = this.role;
      this.userService.createUser(this.newUser).subscribe();
      this.router.navigate(['/users']);
    }
  }

}
