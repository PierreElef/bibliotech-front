import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { UserService } from '../user-service/user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-user-item',
  templateUrl: './user-item.component.html',
  styleUrls: ['./user-item.component.css']
})
export class UserItemComponent implements OnInit {

  @Input() user: User;
  @Output() userHasDeleted = new EventEmitter<boolean>();

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  deleteUser(id) {
    this.userService.deleteUserById(id).subscribe();
    this.userHasDeleted.emit(id);
  }

}
