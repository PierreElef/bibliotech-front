import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, DtoUser } from '../user.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  public getUsers(): Observable<User[]> {
    const url = `${this.url}/users`;
    return this.http.get<User[]>(url, httpOptions);
  }

  public getUserById(id): Observable<User> {
    const url = `${this.url}/users/${id}`;
    return this.http.get<User>(url, httpOptions);
  }

  public createUser(user: DtoUser) {
    const url = `${this.url}/users`;
    return this.http.post<User>(url, user, httpOptions);
  }

  public setUserById(id, user: User): Observable<User> {
    const url = `${this.url}/users/${id}`;
    return this.http.put<User>(url, user, httpOptions);
  }

  public deleteUserById(user: User | number): Observable<User> {
    const id = typeof user === 'number' ? user : user.id;
    const url = `${this.url}/users/${id}`;
    return this.http.delete<User>(url, httpOptions);
  }

}
