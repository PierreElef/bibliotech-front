import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IsLoggedGuard } from './commun/me/is-logged.guard';
import { AuthComponent } from './auth/auth.component';
import { HomepageComponent } from './commun/homepage/homepage.component';

const routes: Routes = [
  {
    path: '',
    component: HomepageComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: 'dashboard',
    loadChildren: () => import('./borrow/borrow.module')
      .then(module => module.BorrowModule)
  },
  {
    canActivate: [IsLoggedGuard],
    path: 'users',
    loadChildren: () => import('./user/user.module')
      .then(module => module.UserModule)
  },
  {
    path: 'auth',
    component: AuthComponent
  },
  {
    path: 'books',
    loadChildren: () => import('./book/book.module')
      .then(module => module.BookModule)
  },
  {
    path: 'authors',
    loadChildren: () => import('./author/author.module')
      .then(module => module.AuthorModule)
  },
  {
    path: 'genres',
    loadChildren: () => import('./genre/genre.module')
      .then(module => module.GenreModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./user/user.module')
      .then(module => module.UserModule)
  },
  {
    path: 'borrow',
    loadChildren: () => import('./borrow/borrow.module')
      .then(module => module.BorrowModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
