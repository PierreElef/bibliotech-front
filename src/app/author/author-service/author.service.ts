import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Author, DtoAuthor } from '../author.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})

export class AuthorService {

  private url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  public getAuthors(): Observable<Author[]> {
    const url = `${this.url}/authors`;
    return this.http.get<Author[]>(url, httpOptions);
  }

  public getAuthorById(id): Observable<Author> {
    const url = `${this.url}/authors/${id}`;
    return this.http.get<Author>(url, httpOptions);
  }

  public createAuthor(author: DtoAuthor) {
    const url = `${this.url}/authors`;
    return this.http.post<Author>(url, author, httpOptions);
  }

  public setAuthorById(id, author: Author): Observable<Author> {
    const url = `${this.url}/authors/${id}`;
    return this.http.put<Author>(url, author, httpOptions);
  }

  public deleteAuthorById(author: Author | number): Observable<Author> {
    const id = typeof author === 'number' ? author : author.id;
    const url = `${this.url}/authors/${id}`;
    return this.http.delete<Author>(url, httpOptions);
  }

  public findAuthor(): Observable<Author[]> {
    const url = `${this.url}/authors/find/author`;
    return this.http.get<Author[]>(url, httpOptions);
  }

}
