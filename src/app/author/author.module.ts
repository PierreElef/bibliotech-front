import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { AuthorRoutingModule } from './author-routing.module';
import { AuthorComponent } from './author.component';
import { AuthorListComponent } from './author-list/author-list.component';
import { AuthorEditComponent } from './author-edit/author-edit.component';
import { AuthorItemComponent } from './author-item/author-item.component';
import { AuthorFormComponent } from './author-form/author-form.component';
import { AuthorCreateComponent } from './author-create/author-create.component';


@NgModule({
  declarations: [
    AuthorComponent,
    AuthorEditComponent,
    AuthorListComponent,
    AuthorItemComponent,
    AuthorFormComponent,
    AuthorCreateComponent
  ],
  imports: [
    CommonModule,
    AuthorRoutingModule,
    FormsModule
  ]
})
export class AuthorModule { }
