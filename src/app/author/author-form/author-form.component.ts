import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Author, DtoAuthor } from '../author.model';
import { AuthorService } from '../author-service/author.service';

@Component({
  selector: 'app-author-form',
  templateUrl: './author-form.component.html',
  styleUrls: ['./author-form.component.css']
})
export class AuthorFormComponent implements OnInit {

  @Input() author: Author;
  newAuthor: DtoAuthor = {
    firstName : '',
    lastName : '',
  };
  firstName:string;
  lastName:string;
  create = false;

  constructor(private authorService: AuthorService, private router: Router) { }

  ngOnInit(): void {
    if (this.author) {
      this.firstName = this.author.firstName;
      this.lastName = this.author.lastName;
    } else {
      this.create = true;
    }
  }

  submit() {
    if (this.author) {
      this.author.firstName = this.firstName;
      this.author.lastName = this.lastName;
      this.authorService.setAuthorById(this.author.id, this.author).subscribe();
      this.router.navigate(['/authors']);
    } else {
      this.newAuthor.firstName = this.firstName;
      this.newAuthor.lastName = this.lastName;
      this.authorService.createAuthor(this.newAuthor).subscribe();
      this.router.navigate(['/authors']);
    }
  }

}
