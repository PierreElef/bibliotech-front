import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from '../commun/me/is-logged.guard';
import { AuthorComponent } from './author.component';
import { AuthorEditComponent } from './author-edit/author-edit.component';
import { AuthorCreateComponent } from './author-create/author-create.component';

const routes: Routes = [
  {
    path: '',
    component: AuthorComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: 'create',
    component: AuthorCreateComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: ':id',
    component: AuthorEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthorRoutingModule { }
