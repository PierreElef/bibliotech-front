import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthorService } from '../author-service/author.service';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-edit',
  templateUrl: './author-edit.component.html',
  styleUrls: ['./author-edit.component.css']
})
export class AuthorEditComponent implements OnInit {

  id: number;
  author: Author;

  constructor(private route: ActivatedRoute, private authorService: AuthorService) { }

  ngOnInit(): void {
    this.id = parseInt(this.route.snapshot.params.id, 10);
    this.authorService.getAuthorById(this.id).subscribe((author) => {
      this.author = author;
    })
  }

}
