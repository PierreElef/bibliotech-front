import { Component, Input, OnInit } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { AuthorService } from '../author-service/author.service';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-list',
  templateUrl: './author-list.component.html',
  styleUrls: ['./author-list.component.css']
})
export class AuthorListComponent implements OnInit {

  @Input() me: IAuthMeDto;
  @Input() authors : Author[];

  constructor(private authorService: AuthorService) { }

  ngOnInit(): void {
  }

}
