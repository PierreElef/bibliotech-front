import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MeService } from '../commun/me/me.service';
import { IAuthMeDto } from '../commun/resource/auth/auth.dto';
import { AuthorService } from './author-service/author.service';
import { Author } from './author.model';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {

  me : IAuthMeDto;
  authors : Author[];
  navigationSubscription;
  
  constructor(private meService: MeService, private authorService: AuthorService, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit(): void {
    this.meService.getMe().subscribe((me)=>{
      if(me){
        this.me = me;
      }else{
        this.me = {
          id:0,
          lastName : '',
          firstName : '',
          role : 'VISITOR',
          email: ''
        }
      }
    });
    this.authorService.getAuthors().subscribe((authors) => {
        this.authors = authors;
    })
  }

}
