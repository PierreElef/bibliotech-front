import { Book } from '../book/book.model';

export interface Author {
  id: number,
  firstName: string;
  lastName: string;
  books : Book[];
}

export interface DtoAuthor {
  firstName: string;
  lastName: string;
}

