import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { AuthorService } from '../author-service/author.service';
import { Author } from '../author.model';

@Component({
  selector: 'app-author-item',
  templateUrl: './author-item.component.html',
  styleUrls: ['./author-item.component.css']
})
export class AuthorItemComponent implements OnInit {

  @Input() author: Author;
  @Input() me: IAuthMeDto;

  constructor(private authorService: AuthorService, private router: Router) { }

  ngOnInit(): void {  }

  deleteAuthor(id) {
    this.authorService.deleteAuthorById(id).subscribe();
    this.router.navigate(['/authors']);
  }
}
