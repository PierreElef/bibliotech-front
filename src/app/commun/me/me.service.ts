import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { IAuthMeDto, IAuthTokenDto } from '../resource/auth/auth.dto';
import { AuthResource } from '../resource/auth/auth.resource';
import { AuthTokenService } from './auth-token.service';

@Injectable()
export class MeService {

  private me$ = new BehaviorSubject<IAuthMeDto | null>(null);

  constructor(
    private authTokenService: AuthTokenService,
    private authResource: AuthResource
  ) {
    if (this.authTokenService.getToken()) {
      this.setMe();
    }
  }

  login(tokenDto: IAuthTokenDto): void {
    this.authTokenService.setToken(tokenDto.token);
    this.setMe();
  }

  private setMe(): void {
    this.authResource.me().subscribe(meDto => {
      this.me$.next(meDto);
    });
  }

  getMe(): Observable<IAuthMeDto | null> {
    return this.me$.asObservable();
  }

  logout(): void {
    this.authTokenService.clearToken();
    this.me$.next(null);
  }
}
