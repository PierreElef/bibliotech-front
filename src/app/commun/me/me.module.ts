import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthTokenService } from './auth-token.service';
import { MeService } from './me.service';

@NgModule({
  imports: [
    CommonModule
  ],
  providers: [
    AuthTokenService,
    MeService
  ]
})
export class MeModule { }
