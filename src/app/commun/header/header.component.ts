import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MeService } from 'src/app/commun/me/me.service';
import { IAuthMeDto } from '../resource/auth/auth.dto';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  me: IAuthMeDto;

  constructor(private meService: MeService, private router: Router) { }

  ngOnInit(): void {
    this.meService.getMe().subscribe((me)=>{
      this.me = me;
    });
  }

  logout(): void{
    this.meService.logout();
    console.log('deconnection');
    this.router.navigate(['']);
  }

}
