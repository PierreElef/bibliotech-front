import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/book/book-service/book.service';
import { Book } from 'src/app/book/book.model';
import * as moment from 'moment';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  newBooks: Book[];

  constructor(private bookService: BookService) { }

  ngOnInit(): void {
    const date = new Date();
    const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
    this.bookService.getBooks().subscribe((books) => {
      this.newBooks = books.filter((book) => {
        const dateBook = new Date(book.createdAt);
        if(dateBook >= firstDay){
          return book;
        }
      });
    })
  }

}
