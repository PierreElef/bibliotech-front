import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IAuthLoginCredentials } from './auth.model';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  providers: [
    AuthService
  ]
})
export class AuthComponent {

  formGroup: FormGroup;

  constructor(
    private fb: FormBuilder,
    private service: AuthService
  ) {
    this.formGroup = this.fb.group({
      email: [undefined, [Validators.required, Validators.email]],
      password: [undefined, Validators.required]
    });
  }

  login(): void {
    if (this.formGroup.valid) {
      const credentials: IAuthLoginCredentials = this.formGroup.value;
      this.service.login(credentials).subscribe();
    }
  }

}
