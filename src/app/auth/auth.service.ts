import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { MeService } from '../commun/me/me.service';
import { IAuthCredentialsDto } from '../commun/resource/auth/auth.dto';
import { AuthResource } from '../commun/resource/auth/auth.resource';
import { IAuthLoginCredentials } from './auth.model';

@Injectable()
export class AuthService {

  constructor(
    private authResource: AuthResource,
    private meService: MeService,
    private router: Router
  ) {
  }

  login(credentials: IAuthLoginCredentials): Observable<any> {
    const dto = this.modelToDto(credentials);
    return this.authResource.login(dto)
      .pipe(
        tap(tokenDto => this.meService.login(tokenDto)),
        tap(() => this.router.navigate(['/books']))
      );
  }

  private modelToDto(model: IAuthLoginCredentials): IAuthCredentialsDto {
    return {
      login: model.email,
      pwd: model.password
    };
  }
}
