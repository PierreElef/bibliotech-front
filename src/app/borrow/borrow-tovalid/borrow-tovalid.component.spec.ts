import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowTovalidComponent } from './borrow-tovalid.component';

describe('BorrowTovalidComponent', () => {
  let component: BorrowTovalidComponent;
  let fixture: ComponentFixture<BorrowTovalidComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BorrowTovalidComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowTovalidComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
