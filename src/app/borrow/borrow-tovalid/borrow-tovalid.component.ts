import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Borrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-tovalid',
  templateUrl: './borrow-tovalid.component.html',
  styleUrls: ['./borrow-tovalid.component.css']
})
export class BorrowTovalidComponent implements OnInit {

  @Input() borrowsToValid: Borrow[];
  type = 'toValid';
  @Output() refresh = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void { }

  refreshList(){
    this.refresh.emit();
  }
}
