import { Component, Input, OnInit } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { BorrowService } from '../borrow-service/borrow.service';
import { Borrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-list',
  templateUrl: './borrow-list.component.html',
  styleUrls: ['./borrow-list.component.css']
})
export class BorrowListComponent implements OnInit {

  @Input() me: IAuthMeDto;
  borrows: Borrow[];
  borrowsToValid: Borrow[] = [];
  borrowsOK: Borrow[] = [];
  borrowsLate: Borrow[] = [];
  borrowsReturn: Borrow[] = [];

  constructor(private borrowService: BorrowService) { }

  ngOnInit(): void {
    const today15 = new Date();
    today15.setDate(today15.getDate() - 15);
    this.borrowService.getBorrows().subscribe((borrows) => {
      this.borrows = borrows;
      this.borrowsToValid = this.borrows.filter((borrow) => {
        if (borrow.date_begin == null) {
          return borrow;
        }
      });
      this.borrowsOK = this.borrows.filter((borrow) => {
        if (borrow.date_begin !== null) {
          const dateBorrow = new Date(borrow.date_begin);
          if (dateBorrow >= today15 && borrow.date_end == null) {
            return borrow;
          }
        }
      });
      this.borrowsLate = this.borrows.filter((borrow) => {
        if (borrow.date_begin !== null) {
          const dateBorrow = new Date(borrow.date_begin);
          if (dateBorrow < today15 && borrow.date_end == null) {
            return borrow;
          }
        }
      });
      this.borrowsReturn = this.borrows.filter((borrow) => {
        if (borrow.date_end !== null) {
            return borrow;
        }
      });
    });
  }

  refreshList(){
    this.ngOnInit();
  }

}
