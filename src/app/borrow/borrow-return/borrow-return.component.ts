import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { Borrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-return',
  templateUrl: './borrow-return.component.html',
  styleUrls: ['./borrow-return.component.css']
})
export class BorrowReturnComponent implements OnInit {

  @Input() borrowsReturn: Borrow[];
  type = 'return';
  @Input() me: IAuthMeDto;
  @Output() refresh = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {}

  refreshList(){
    this.refresh.emit();
  }
}
