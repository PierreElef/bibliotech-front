import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BorrowComponent } from './borrow.component';
import { BorrowListComponent } from './borrow-list/borrow-list.component';
import { BorrowTovalidComponent } from './borrow-tovalid/borrow-tovalid.component';
import { BorrowLateComponent } from './borrow-late/borrow-late.component';
import { BorrowOkComponent } from './borrow-ok/borrow-ok.component';
import { BorrowItemComponent } from './borrow-item/borrow-item.component';
import { BorrowRoutingModule } from './borrow-routing.module';
import { BorrowReturnComponent } from './borrow-return/borrow-return.component';

@NgModule({
  declarations: [
    BorrowComponent,
    BorrowListComponent,
    BorrowTovalidComponent,
    BorrowLateComponent,
    BorrowOkComponent,
    BorrowItemComponent,
    BorrowReturnComponent
  ],
  imports: [
    CommonModule,
    BorrowRoutingModule
  ]
})
export class BorrowModule { }
