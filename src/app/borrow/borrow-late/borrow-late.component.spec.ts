import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowLateComponent } from './borrow-late.component';

describe('BorrowLateComponent', () => {
  let component: BorrowLateComponent;
  let fixture: ComponentFixture<BorrowLateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BorrowLateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowLateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
