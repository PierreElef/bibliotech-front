import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { Borrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-late',
  templateUrl: './borrow-late.component.html',
  styleUrls: ['./borrow-late.component.css']
})
export class BorrowLateComponent implements OnInit {

  @Input() borrowsLate: Borrow[];
  @Input() me: IAuthMeDto;
  type = 'late';
  @Output() refresh = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {   }

  refreshList(){
    this.refresh.emit();
  }

}
