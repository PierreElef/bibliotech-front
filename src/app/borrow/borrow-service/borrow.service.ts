import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Borrow, DtoBorrow } from '../borrow.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})

export class BorrowService {

  private url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  public getBorrows(): Observable<Borrow[]> {
    const url = `${this.url}/borrows`;
    return this.http.get<Borrow[]>(url, httpOptions);
  }

  public getBorrowById(id): Observable<Borrow> {
    const url = `${this.url}/borrows/${id}`;
    return this.http.get<Borrow>(url, httpOptions);
  }

  public createBorrow(borrow: DtoBorrow) {
    const url = `${this.url}/borrows`;
    return this.http.post<Borrow>(url, borrow, httpOptions);
  }

  public setBorrowById(id, borrow: Borrow): Observable<Borrow> {
    const url = `${this.url}/borrows/${id}`;
    return this.http.put<Borrow>(url, borrow, httpOptions);
  }

  public deleteBorrowById(borrow: Borrow | number): Observable<Borrow> {
    const id = typeof borrow === 'number' ? borrow : borrow.id;
    const url = `${this.url}/borrows/${id}`;
    return this.http.delete<Borrow>(url, httpOptions);
  }

  public findBorrow(): Observable<Borrow[]> {
    const url = `${this.url}/borrows/find/borrow`;
    return this.http.get<Borrow[]>(url, httpOptions);
  }

}
