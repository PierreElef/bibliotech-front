import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { BorrowService } from '../borrow-service/borrow.service';
import { Borrow, DtoBorrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-item',
  templateUrl: './borrow-item.component.html',
  styleUrls: ['./borrow-item.component.css']
})
export class BorrowItemComponent implements OnInit {

  @Input() borrow: Borrow;
  @Input() type: string;
  @Input() me: IAuthMeDto;
  @Output() refresh = new EventEmitter<boolean>();
  date: string;

  constructor(private borrowService: BorrowService) { }

  ngOnInit(): void {
    if (this.type === 'return') {
      const dateEnd = new Date(this.borrow.date_end);
      this.date = this.formattedDate(dateEnd);
    } else {
      const dateStart = new Date(this.borrow.date_begin);
      this.date = this.formattedDate(dateStart);
    }
  }

  formattedDate(d: Date) {
    let month = String(d.getMonth() + 1);
    let day = String(d.getDate());
    const year = String(d.getFullYear());
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return `${day}/${month}/${year}`;
  }

  validBorrow(){
    const dateNow = new Date()
    this.borrow.date_begin = dateNow;
    this.borrowService.setBorrowById(this.borrow.id, this.borrow).subscribe();
    this.refresh.emit();
  }

  returnBorrow(){
    const dateNow = new Date()
    this.borrow.date_end = dateNow;
    this.borrowService.setBorrowById(this.borrow.id, this.borrow).subscribe();
    this.refresh.emit();
  }

}

