import { Book } from '../book/book.model';
import { User } from '../user/user.model';


export interface Borrow {
  id:number,
  user_id: number,
  book_id: number,
  date_begin: Date,
  date_end: Date,
  user: User;
  book: Book;
}

export interface DtoBorrow {
  user_id: number,
  book_id: number,
  date_begin: Date,
  date_end: Date,
}
