import { Component, OnInit } from '@angular/core';
import { MeService } from 'src/app/commun/me/me.service';
import { IAuthMeDto } from '../commun/resource/auth/auth.dto';

@Component({
  selector: 'app-borrow',
  templateUrl: './borrow.component.html',
  styleUrls: ['./borrow.component.css']
})
export class BorrowComponent implements OnInit {

  me: IAuthMeDto;

  constructor(private meService: MeService) { }

  ngOnInit(): void {
    this.meService.getMe().subscribe((me)=>{
      this.me = me;
    });
  }

}
