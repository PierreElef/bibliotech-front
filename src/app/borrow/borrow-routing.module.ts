import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from '../commun/me/is-logged.guard';
import { BorrowComponent } from './borrow.component';

const routes: Routes = [
  {
    path: '',
    component: BorrowComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BorrowRoutingModule { }
