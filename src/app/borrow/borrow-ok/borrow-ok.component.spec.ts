import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BorrowOkComponent } from './borrow-ok.component';

describe('BorrowOkComponent', () => {
  let component: BorrowOkComponent;
  let fixture: ComponentFixture<BorrowOkComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BorrowOkComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BorrowOkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
