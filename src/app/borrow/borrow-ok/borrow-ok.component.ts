import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { Borrow } from '../borrow.model';

@Component({
  selector: 'app-borrow-ok',
  templateUrl: './borrow-ok.component.html',
  styleUrls: ['./borrow-ok.component.css']
})
export class BorrowOkComponent implements OnInit {

  @Input() borrowsOK: Borrow[];
  @Input() me: IAuthMeDto;
  type = 'ok';
  @Output() refresh = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {   }

  refreshList(){
    this.refresh.emit();
  }
}
