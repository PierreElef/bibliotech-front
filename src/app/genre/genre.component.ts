import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MeService } from '../commun/me/me.service';
import { IAuthMeDto } from '../commun/resource/auth/auth.dto';
import { GenreService } from './genre-service/genre.service';
import { Genre } from './genre.model';

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {

  me : IAuthMeDto;
  genres: Genre[];

  navigationSubscription;
  
  constructor(private meService: MeService, private genreService: GenreService, private router: Router) {
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }
  ngOnInit(): void {
    this.meService.getMe().subscribe((me)=>{
      if(me){
        this.me = me;
      }else{
        this.me = {
          id:0,
          lastName : '',
          firstName : '',
          role : 'VISITOR',
          email: ''
        }
      }
    });
    this.genreService.getGenres().subscribe((genres) => {
      this.genres = genres;
    })
  }

}
