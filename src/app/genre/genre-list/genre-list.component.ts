import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { GenreService } from '../genre-service/genre.service';
import { Genre } from '../genre.model';

@Component({
  selector: 'app-genre-list',
  templateUrl: './genre-list.component.html',
  styleUrls: ['./genre-list.component.css']
})
export class GenreListComponent implements OnInit {

  @Input() genres: Genre[];
  @Input() me: IAuthMeDto;

  constructor(private genreService: GenreService, private router: Router) { }

  ngOnInit(): void {
  }

}
