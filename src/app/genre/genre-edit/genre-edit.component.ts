import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenreService } from '../genre-service/genre.service';
import { Genre } from '../genre.model';

@Component({
  selector: 'app-genre-edit',
  templateUrl: './genre-edit.component.html',
  styleUrls: ['./genre-edit.component.css']
})
export class GenreEditComponent implements OnInit {

  id:any;
  genre: Genre;

  constructor(private route: ActivatedRoute, private genreService: GenreService) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params.id;
    this.genreService.getGenreById(this.id).subscribe((genre) => {
      this.genre = genre;
    })
  }

}
