import { Book } from '../book/book.model';

export interface Genre {
  id:number,
  libel: string,
  books : Book[]
}
export interface DtoGenre {
  libel: string
}
