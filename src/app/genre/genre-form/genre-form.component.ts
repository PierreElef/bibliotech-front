import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Genre, DtoGenre } from '../genre.model';
import { GenreService } from '../genre-service/genre.service';

@Component({
  selector: 'app-genre-form',
  templateUrl: './genre-form.component.html',
  styleUrls: ['./genre-form.component.css']
})
export class GenreFormComponent implements OnInit {

  @Input() genre: Genre;
  newGenre: DtoGenre = {
    libel : '',
  };
  libel = '';
  create = false;

  constructor(private genreService: GenreService, private router: Router) { }

  ngOnInit(): void {
    if (this.genre) {
      this.libel = this.genre.libel;
    } else {
      this.create = true;
    }
  }

  submit() {
    if (this.genre) {
      this.genre.libel = this.libel;
      this.genreService.setGenreById(this.genre.id, this.genre).subscribe();
      this.router.navigate(['/genres']);
    } else {
      this.newGenre.libel = this.libel;
      this.genreService.createGenre(this.newGenre).subscribe();
      this.router.navigate(['/genres']);
    }
  }
}
