import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Genre, DtoGenre } from '../genre.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class GenreService {

  private url = 'http://localhost:3000';

  constructor(private http: HttpClient) { }

  public getGenres(): Observable<Genre[]> {
    const url = `${this.url}/genres`;
    return this.http.get<Genre[]>(url, httpOptions);
  }

  public getGenreById(id): Observable<Genre> {
    const url = `${this.url}/genres/${id}`;
    return this.http.get<Genre>(url, httpOptions);
  }

  public createGenre(genre: DtoGenre) {
    const url = `${this.url}/genres`;
    return this.http.post<Genre>(url, genre, httpOptions);
  }

  public setGenreById(id, genre: Genre): Observable<Genre> {
    const url = `${this.url}/genres/${id}`;
    return this.http.put<Genre>(url, genre, httpOptions);
  }

  public deleteGenreById(genre: Genre | number): Observable<Genre> {
    const id = typeof genre === 'number' ? genre : genre.id;
    const url = `${this.url}/genres/${id}`;
    return this.http.delete<Genre>(url, httpOptions);
  }

  public findGenre(): Observable<Genre[]> {
    const url = `${this.url}/genres/find/genre`;
    return this.http.get<Genre[]>(url, httpOptions);
  }
}
