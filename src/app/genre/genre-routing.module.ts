import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from '../commun/me/is-logged.guard';
import { GenreComponent } from './genre.component';
import { GenreEditComponent } from './genre-edit/genre-edit.component';
import { GenreCreateComponent } from './genre-create/genre-create.component';

const routes: Routes = [
  {
    path: '',
    component: GenreComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: 'create',
    component: GenreCreateComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: ':id',
    component: GenreEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GenreRoutingModule { }
