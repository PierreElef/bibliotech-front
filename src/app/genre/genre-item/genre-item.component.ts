import { Component, Input,Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { GenreService } from '../genre-service/genre.service';
import { Genre } from '../genre.model';

@Component({
  selector: 'app-genre-item',
  templateUrl: './genre-item.component.html',
  styleUrls: ['./genre-item.component.css']
})
export class GenreItemComponent implements OnInit {

  @Input() genre: Genre;
  nbBooks : number;
  @Input() me: IAuthMeDto;

  constructor(private genreService: GenreService, private router: Router) { }

  ngOnInit(): void {}

  deleteGenre(id) {
    this.genreService.deleteGenreById(id).subscribe();
    this.router.navigate(['/genres']);
  }
}
