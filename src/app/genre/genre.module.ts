import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { GenreComponent } from './genre.component';
import { GenreListComponent } from './genre-list/genre-list.component';
import { GenreEditComponent } from './genre-edit/genre-edit.component';
import { GenreRoutingModule } from './genre-routing.module';
import { GenreItemComponent } from './genre-item/genre-item.component';
import { GenreCreateComponent } from './genre-create/genre-create.component';
import { GenreFormComponent } from './genre-form/genre-form.component';


@NgModule({
  declarations: [
    GenreComponent,
    GenreListComponent,
    GenreEditComponent,
    GenreItemComponent,
    GenreCreateComponent,
    GenreFormComponent
  ],
  imports: [
    CommonModule,
    GenreRoutingModule,
    FormsModule
  ]
})
export class GenreModule { }
