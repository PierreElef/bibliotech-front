import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BorrowService } from 'src/app/borrow/borrow-service/borrow.service';
import { Borrow, DtoBorrow } from 'src/app/borrow/borrow.model';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { BookService } from '../book-service/book.service';
import { Book } from '../book.model';

@Component({
  selector: 'app-book-item',
  templateUrl: './book-item.component.html',
  styleUrls: ['./book-item.component.css']
})
export class BookItemComponent implements OnInit {

  @Input() book : Book;
  @Input() me: IAuthMeDto;
  borrowDto: DtoBorrow = {
    user_id: 0,
    book_id: 0,
    date_begin: null,
    date_end: null
  };
  borrows: Borrow[];
  alreadyBorrow = 'notBorrow';

  constructor(
    private bookService: BookService,
    private router: Router,
    private borrowService: BorrowService
  ) { }

  ngOnInit(): void {
    this.borrows = this.book.borrows;
    this.borrows = this.borrows.filter((borrow) => {
      if (borrow.date_end == null) {
        return borrow;
      }
    });
    this.borrows.forEach((borrow) => {
      if (this.book.id === borrow.book_id) {
        if (this.me.id === borrow.user_id) {
          this.alreadyBorrow = 'borrowByMe';
        } else {
          this.alreadyBorrow = 'borrow';
        }
      }
    })
  }

  deleteBook() {
    this.bookService.deleteBookById(this.book.id).subscribe();
    this.router.navigate(['/books']);
  }

  borrowBook() {
    this.borrowDto.user_id = this.me.id;
    this.borrowDto.book_id = this.book.id;
    this.borrowService.createBorrow(this.borrowDto).subscribe();
    this.alreadyBorrow = 'borrow';
  }

}
