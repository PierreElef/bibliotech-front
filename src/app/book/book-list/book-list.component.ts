import { Component, Input, OnInit } from '@angular/core';
import { BorrowService } from 'src/app/borrow/borrow-service/borrow.service';
import { IAuthMeDto } from 'src/app/commun/resource/auth/auth.dto';
import { Book } from '../book.model';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  @Input() books: Book[];
  @Input() me: IAuthMeDto;

  constructor(private borrowService: BorrowService) { }

  ngOnInit(): void {
  }

}
