import { Author } from '../author/author.model';
import { Borrow } from '../borrow/borrow.model';
import { Genre } from '../genre/genre.model';

export interface Book {
  id:number,
  title: string,
  genre_id: number,
  author_id: number,
  author: Author;
  genre: Genre;
  borrows: Borrow[];
  createdAt: Date;
}

export interface DtoBook {
  id:number,
  title: string,
  genre_id: number,
  author_id: number
  author: Author;
  genre: Genre;
  borrows: Borrow[];
  createdAt: Date;
}
