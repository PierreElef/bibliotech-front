import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { IsLoggedGuard } from '../commun/me/is-logged.guard';
import { BookComponent } from './book.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookCreateComponent } from './book-create/book-create.component';

const routes: Routes = [
  {
    path: '',
    component: BookComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: 'create',
    component: BookCreateComponent
  },
  {
    canActivate: [IsLoggedGuard],
    path: ':id',
    component: BookEditComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookRoutingModule { }
