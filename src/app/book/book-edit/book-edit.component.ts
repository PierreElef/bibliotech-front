import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BookService } from '../book-service/book.service';
import { Book } from '../book.model';

@Component({
  selector: 'app-book-edit',
  templateUrl: './book-edit.component.html',
  styleUrls: ['./book-edit.component.css']
})
export class BookEditComponent implements OnInit {

  id:any;
  book:Book;

  constructor(private route: ActivatedRoute,private bookService: BookService) { }

  ngOnInit(): void {

    this.id = parseInt(this.route.snapshot.params.id, 10);
    this.bookService.getBookById(this.id).subscribe((book) => {
      this.book = book;
    })
  }

}
