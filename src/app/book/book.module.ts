import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { BookComponent } from './book.component';
import { BookListComponent } from './book-list/book-list.component';
import { BookEditComponent } from './book-edit/book-edit.component';
import { BookRoutingModule } from './book-routing.module';
import { BookItemComponent } from './book-item/book-item.component';
import { BookFormComponent } from './book-form/book-form.component';
import { BookCreateComponent } from './book-create/book-create.component';

@NgModule({
  declarations: [
    BookComponent,
    BookListComponent,
    BookEditComponent,
    BookItemComponent,
    BookFormComponent,
    BookCreateComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BookRoutingModule,
  ]
})
export class BookModule { }
