import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { MeService } from '../commun/me/me.service';
import { IAuthMeDto } from '../commun/resource/auth/auth.dto';
import { BookService } from './book-service/book.service';
import { Book } from './book.model';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  books: Book[];
  navigationSubscription;
  me : IAuthMeDto;

  constructor(private bookService: BookService, private router: Router, private meService: MeService) {
    this.navigationSubscription = this.router.events.subscribe((event: any) => {
      if (event instanceof NavigationEnd) {
        this.ngOnInit();
      }
    });
  }

  ngOnInit(): void {
    this.meService.getMe().subscribe((me)=>{
      if(me){
        this.me = me;
      }else{
        this.me = {
          id:0,
          lastName : '',
          firstName : '',
          role : 'VISITOR',
          email: ''
        }
      }
    });
    this.bookService.getBooks().subscribe((books) => {
      this.books = books;
      this.books.sort((bookA: Book, bookB: Book) => {
        return bookA.id - bookB.id;
      })
    });
  }

  reload(): void {
    this.ngOnInit()
  }

}
