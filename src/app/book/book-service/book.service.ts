import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Book } from '../book.model';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*' })
};

@Injectable({
  providedIn: 'root'
})
export class BookService {

  private url = 'http://localhost:3000'

  constructor(private http: HttpClient) { }

  public getBooks(): Observable<Book[]>{
    const url = `${this.url}/books`;
    return this.http.get<Book[]>(url, httpOptions);
  }

  public getBookById(id): Observable<Book>{
    const url = `${this.url}/books/${id}`;
    return this.http.get<Book>(url, httpOptions);
  }

  public createBook(book: Book) {
    const url = `${this.url}/books`;
    return this.http.post<Book>(url, book, httpOptions);
  }

  public setBookById(id, book: Book): Observable<Book> {
    const url = `${this.url}/books/${id}`;
    return this.http.put<Book>(url, book, httpOptions);
  }

  public deleteBookById(book: Book | number): Observable<Book> {
    const id = typeof book === 'number' ? book : book.id;
    const url = `${this.url}/books/${id}`;
    return this.http.delete<Book>(url, httpOptions);
  }

}
