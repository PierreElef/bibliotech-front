import { Component,Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book, DtoBook } from '../book.model';
import { Author, DtoAuthor } from '../../author/author.model';
import { Genre, DtoGenre } from '../../genre/genre.model';
import { BookService } from '../book-service/book.service';
import { AuthorService } from '../../author/author-service/author.service';
import { GenreService } from '../../genre/genre-service/genre.service';

@Component({
  selector: 'app-book-form',
  templateUrl: './book-form.component.html',
  styleUrls: ['./book-form.component.css']
})
export class BookFormComponent implements OnInit {

  @Input() book: Book;
  newBook: DtoBook = {
    id:null,
    title : '',
    genre_id: 0,
    author_id: 0,
    author: null,
    genre: null,
    borrows: null,
    createdAt: null
  };
  authors:Author[];
  author:Author;
  genres:Genre[];
  genre:Genre;
  title:string;
  genreId: number;
  authorId: number;
  create = false;


  constructor(
    private bookService: BookService,
    private authorService: AuthorService,
    private genreService: GenreService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.authorService.getAuthors().subscribe((authors) => {
      this.authors = authors;
    })
    this.genreService.getGenres().subscribe((genres) => {
      this.genres = genres;
    })

    if(!this.book) {
      this.create = true;
    }else{
      this.title = this.book.title;
      this.genreId = this.book.genre_id;
      this.authorId = this.book.author_id;
    }

  }

  submit(){
    if(this.create){
      this.newBook.title = this.title;
      this.newBook.genre_id = this.genreId;
      this.newBook.author_id = this.authorId;
      this.authorService.getAuthorById(this.authorId).subscribe((author) => {
        this.newBook.author = author;
      })
      this.genreService.getGenreById(this.genreId).subscribe((genre) => {
        this.newBook.genre = genre;
      })
      this.newBook.createdAt = new Date()
      this.bookService.createBook(this.newBook).subscribe()
      this.router.navigate(['/books']);
    }else{
      this.book.title = this.title;
      this.book.genre_id = this.genreId;
      this.book.author_id = this.authorId;
      this.bookService.setBookById(this.book.id, this.book).subscribe()
      this.router.navigate(['/books']);
    }
  }

}
